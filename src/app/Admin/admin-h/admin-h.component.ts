import { Component, DoCheck} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Reservation } from 'src/app/model/reservation';

@Component({
  selector: 'app-admin-h',
  templateUrl: './admin-h.component.html',
  styleUrls: ['./admin-h.component.scss'],
})
export class AdminHComponent implements  DoCheck {
  reservationNotCofirm: number
  reservations: Reservation[]
  constructor(private _route: ActivatedRoute) {    
  }

  ngDoCheck(){
    this.reservations = this._route.snapshot.data['reservations']    
    this.reservationNotCofirm = this.reservations.filter(i => i.confirm === false).length
  }

}
