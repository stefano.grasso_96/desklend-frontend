import { Component, DoCheck, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from 'src/app/services/company.service';
import { Company } from 'src/app/model/company';

@Component({
  selector: 'app-lista-azieDesk',
  templateUrl: './lista-aziende-admin.component.html',
  styleUrls: ['./lista-aziende-admin.component.scss'],
})
export class ListaAziendeAdminComponent implements DoCheck, OnInit {
  companies: Array<Company> = []

  constructor(private serviceCompany: CompanyService,
    private router: Router,
    private alertCtrl: AlertController,
    private _route: ActivatedRoute) {
      
  }
  ngOnInit() {
    this.companies = this._route.snapshot.data['companies']
    
  }

  ngDoCheck() {
    this.companies = this._route.snapshot.data['companies']
  }


  delete(company: Company) {
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this azienda?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Delete', handler: () => {
          this.serviceCompany.deleteAzienda(company);

          let index = this.companies.indexOf(company);
          if (index >= 0) {
            this.companies.splice(index, 1);//index=da dove parto e i= la quantità di elementi da eliminare 
          }

        }
      }
      ]
    }).then(alertEl => { alertEl.present() })

  }

  add() {
    this.router.navigate(['new-company'])
  }
  modify(company: Company) {
    localStorage.setItem('idAziendaAdmin', company.id.toString())
    this.router.navigate(['lista-sedi-admin'])
  }

}


