import { Component, OnInit, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Desk } from 'src/app/model/desk';
import { DeskService } from 'src/app/services/desk.service';
import { Room } from 'src/app/model/room';
import { FormBuilder, Validators } from '@angular/forms';
import { RoomsService } from 'src/app/services/rooms.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-lista-desk-admin',
  templateUrl: './lista-desk-admin.component.html',
  styleUrls: ['./lista-desk-admin.component.scss'],
})
export class ListaDeskAdminComponent implements OnInit, DoCheck {
  desks: Array<Desk> = []
  formUpdateStanza
  SVGFile1: SafeUrl
  room: Room
  rooms:Room[]
  constructor(public fb: FormBuilder,
    private serviceDesk: DeskService,
    private alertCtrl: AlertController,
    private router: Router,
    protected sanitizer: DomSanitizer,
    private serviceRoom: RoomsService,
    private _activatedRoute: ActivatedRoute) {
      this.rooms=this._activatedRoute.snapshot.data['rooms']
      this.desks=this._activatedRoute.snapshot.data['desks']
    
  }

  ngOnInit() {
    this.rooms=this._activatedRoute.snapshot.data['rooms']
      this.desks=this._activatedRoute.snapshot.data['desks']
    this.desks = this.desks.filter(i => i.idRoom === +(localStorage.getItem('idStanzaAdmin')))
    this.room = this.rooms.find(i=>i.idRoom==+localStorage.getItem('idStanzaAdmin'))
    this.SVGFile1 = this.sanitizer.bypassSecurityTrustResourceUrl('assets/Room_' + "2" + '.svg');
    this.formUpdateStanza = this.fb.group({

      'nameStanza': [this.room.name, [Validators.required, Validators.minLength(2)]],

    })
  }

  ngDoCheck(){
    this.desks=this._activatedRoute.snapshot.data['desks']
    this.desks = this.desks.filter(i => i.idRoom === +(localStorage.getItem('idStanzaAdmin')))
  }

  delete(desk: Desk) {
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this desk?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Delete', handler: () => {
          this.serviceDesk.deleteDesk(desk).subscribe(()=>window.location.reload());          
        }
      }
      ]
    }).then(alertEl => { alertEl.present() })
   


  }
  add() {
    this.router.navigate(['new-desk'])
  }
  modify(desk:Desk) {
    localStorage.setItem('idDeskAdmin', desk.idDesk.toString())
    this.router.navigate(['modify-desk'])
  }

  updateStanza() {

    this.room.name = this.formUpdateStanza.get('nameStanza').value
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to update this stanza?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Modify', handler: () => {
          this.serviceRoom.updateRoom(this.room).subscribe(res=>this.router.navigate(['lista-stanze-admin']));
          
        }
      }]
    }).then(alertEl => { alertEl.present() })
  }

  antani(){
    let img = document.getElementById('idObjectSVG')
    let iframe = (<HTMLObjectElement>img).contentDocument;
  }


}
