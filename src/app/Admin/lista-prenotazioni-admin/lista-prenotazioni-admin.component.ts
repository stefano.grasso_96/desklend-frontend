import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Company } from 'src/app/model/company';
import { Reservation } from 'src/app/model/reservation';
import { User } from 'src/app/model/user';
import { ReservationService } from 'src/app/services/reservation.service';


@Component({
  selector: 'app-lista-prenotazioni-admin',
  templateUrl: './lista-prenotazioni-admin.component.html',
  styleUrls: ['./lista-prenotazioni-admin.component.scss'],
})
export class ListaPrenotazioniAdminComponent implements OnInit {
  reservations: Array<Reservation> = []
  companies:Company[]
  users:User[]
  constructor(private reservationService: ReservationService,
     private alertCtrl: AlertController,
     private _route:ActivatedRoute) {
      this.companies= this._route.snapshot.data['companies']
      this.users=this._route.snapshot.data['users']
      this.reservations=this._route.snapshot.data['reservations']
      
      }

  ngOnInit() {
    
    
    this.reservations = this.reservations.filter(i => i.confirm == false).sort((a, b) => {
      if (a.idReservation > b.idReservation) {
        return 1;
      }
      if (a.idReservation < b.idReservation) {
        return -1;
      }
      return 0;
    })

  }

cercaUtente(idUtente){
return this.users.find(i=>i.id==idUtente)
}
cercaAzienda(idUtente){
  let idA=this.cercaUtente(idUtente).idCompany
  return this.companies.find(i=>i.id==idA)
  }

  delete(reservation: Reservation) {
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this reservation?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Delete', handler: () => {
          this.reservationService.deleteReservation(reservation).subscribe(()=>window.location.reload());

          this.reservations = this.reservations.filter(i => i.confirm == false).sort((a, b) => {
            if (a.idReservation > b.idReservation) {
              return 1;
            }
            if (a.idReservation < b.idReservation) {
              return -1;
            }
            return 0;
          })
        }
      }
      ]
    }).then(alertEl => { alertEl.present() })
    
  }

  confirm(reservation: Reservation) {
    this.reservationService.confirmReservation(reservation)
  }

}
