import {  Component, DoCheck, OnInit} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Company } from 'src/app/model/company';
import { Desk } from 'src/app/model/desk';
import { Office } from 'src/app/model/office';
import { CompanyService } from 'src/app/services/company.service';
import { OfficeService } from 'src/app/services/office.service';
import { Room } from './../../model/room';
import { Reservation } from './../../model/reservation';
import * as moment from 'moment';

@Component({
  selector: 'app-lista-sedi-admin',
  templateUrl: './lista-sedi-admin.component.html',
  styleUrls: ['./lista-sedi-admin.component.scss'],
})
export class ListaSediAdminComponent implements OnInit, DoCheck {
  sedi: Array<Office> = []
  formUpdateAzienda
  company: Company
  companies: Company[]
  rooms:Room[]
  desks:Desk[]
  reservations:Reservation[]
  
  constructor(
    private alertCtrl: AlertController,
    private router: Router,
    private serviceCompany: CompanyService,
    private office: OfficeService,
    public fb: FormBuilder,
    private _activatedRoute: ActivatedRoute) {
    this.companies = this._activatedRoute.snapshot.data['companies'];
    this.sedi = this._activatedRoute.snapshot.data['officies'];
    this.desks=this._activatedRoute.snapshot.data['desks'];
    this.rooms=this._activatedRoute.snapshot.data['rooms'];
    this.reservations=this._activatedRoute.snapshot.data['reservations']
  }

  ngOnInit() {
    this.companies = this._activatedRoute.snapshot.data['companies'];
    this.sedi = this._activatedRoute.snapshot.data['officies']
    this.sedi = this.sedi.filter(i => i.idCompany == +(localStorage.getItem('idAziendaAdmin')))
    this.company = this.companies.find(i => i.id == +localStorage.getItem('idAziendaAdmin'))
    this.formUpdateAzienda = this.fb.group({

      'nameAzienda': [this.company.name, [Validators.required, Validators.minLength(2)]],

    })
  }

  ngDoCheck() {
    this.sedi = this._activatedRoute.snapshot.data['officies']
    this.sedi = this.sedi.filter(i => i.idCompany == +(localStorage.getItem('idAziendaAdmin')))
  }
 

  delete(sede: Office) {
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this sede?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Delete', handler: () => {
          this.office.deleteSede(sede).subscribe(() => {
            window.location.reload();
          });

        }
      }
      ]
    }).then(alertEl => { alertEl.present() })
  }



    totalePostiperOffice(sede:Office){
      let stanzePerOffice=this.rooms.filter(i=>i.idOffice== sede.idOffice)
      let x=0;
     this.desks.forEach(element1 => {
       stanzePerOffice.forEach(element2 => {
         if(element1.idRoom==element2.idRoom){
           x=x+1;
         }
       });
     });
      return x 
     }
     
      postiDisponibili(stanza:Room){
      let deskInRoom=this.desks.filter(i=>i.idRoom==stanza.idRoom);
      let reservationNow= this.reservations.filter(i=>moment(i.dataOraIn)<moment() && moment(i.dataOraOut)>moment())
      
      let x=0;
      deskInRoom.forEach(element1 => {
        reservationNow.forEach(element2 => {
          if(element1.idDesk==element2.idDesk){
            x=x+1;
          }          
        });        
      });      
      return deskInRoom.length-x;
    }
     postiDisponibiliInSede(sede:Office){
       let stanzePerOffice=this.rooms.filter(i=>i.idOffice== sede.idOffice)
       let x=0;
       stanzePerOffice.forEach(element => {
         x=x+this.postiDisponibili(element)         
       });
       return x
     }

    

  
  add() {
    this.router.navigate(['new-office'])
  }
  modify(sede: Office) {
    localStorage.setItem('idSedeAdmin', sede.idOffice.toString())
    this.router.navigate(['lista-stanze-admin'])
  }

  updateAzienda() {
    this.company.name = this.formUpdateAzienda.get('nameAzienda').value
    this.serviceCompany.updateCompany(this.company).subscribe(() => this.router.navigate(['lista-aziende-admin']));
  }
}
