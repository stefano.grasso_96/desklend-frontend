import { AfterViewInit, Component, DoCheck, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Room } from 'src/app/model/room';
import { OfficeService } from 'src/app/services/office.service';
import { RoomsService } from 'src/app/services/rooms.service';
import { Office } from 'src/app/model/office';
import { Desk } from 'src/app/model/desk';
import { Reservation } from 'src/app/model/reservation';
import * as moment from 'moment';
import {Chart}  from 'chart.js';
//import * as Chart from 'chart.js';


@Component({
  selector: 'app-lista-stanze-admin',
  templateUrl: './lista-stanze-admin.component.html',
  styleUrls: ['./lista-stanze-admin.component.scss'],
})
export class ListaStanzeAdminComponent implements OnInit, DoCheck, AfterViewInit {
  stanze: Array<Room> = []
  desks: Desk[]
  officies: Office[]
  office: Office
  reservations: Reservation[]
  formUpdateSede
  buffer = []
  daysX = []
  valueY = []
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //GRAFICO  
  @ViewChild('lineCanvas') private lineCanvas: ElementRef;
  lineChart: any;

  constructor(public fb: FormBuilder,
    private alertCtrl: AlertController,
    private router: Router,
    private roomService: RoomsService,
    private serviceOffice: OfficeService,
    private _activatedRoute: ActivatedRoute) {
    this.officies = this._activatedRoute.snapshot.data['officies'];
    this.stanze = this._activatedRoute.snapshot.data['rooms'];
    this.desks = this._activatedRoute.snapshot.data['desks'];
    this.reservations = this._activatedRoute.snapshot.data['reservations'];
  }

  ngOnInit() {
    this.officies = this._activatedRoute.snapshot.data['officies'];
    this.stanze = this._activatedRoute.snapshot.data['rooms']
    this.stanze = this.stanze.filter(i => i.idOffice === +(localStorage.getItem('idSedeAdmin')))
    this.office = this.officies.find(i => i.idOffice === +localStorage.getItem('idSedeAdmin'))


    this.formUpdateSede = this.fb.group({

      'citta': [this.office.city, [Validators.required, Validators.minLength(2)]],
      'via': [this.office.street, [Validators.required, Validators.minLength(2)]],
      'civico': [this.office.civicNumber, [Validators.required, Validators.minLength(2)]],
      'cap': [this.office.cap, [Validators.required, Validators.minLength(6)]],
      'telefono': [this.office.telefone, [Validators.required, Validators.minLength(6)]]
    })

  }
  ngDoCheck() {
    this.stanze = this._activatedRoute.snapshot.data['rooms']
    this.stanze = this.stanze.filter(i => i.idOffice === +(localStorage.getItem('idSedeAdmin')))
  }

  ngAfterViewInit() {
    this.getDays()
    this.contaPrenotazioniGiorno(moment())
    this.arrayDaInserireInData()
    this.lineChartMethod();
  }

  getDays() {
    var date = new Date();
    let i = 0;
    while (i < 16) {
      this.daysX.push(new Date(date).getDate());
      date.setDate(date.getDate() + 1);
      i++;
    }
    return this.daysX;
  }


  lineChartMethod() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.daysX,
        datasets: [
          {
            label: 'Reserved desks',
            fill: true,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.valueY,
            spanGaps: false,
          }

        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    }
    );
  }




  //metodo per ottenere array con posti prenotati confermati di tutto il mese
  contaPrenotazioniGiorno(giorno) {
    this.desks.forEach(element1 => {
      this.stanze.forEach(element2 => {
        if (element1.idRoom == element2.idRoom) {
          this.buffer.push(element1);
        }
      })
    })
    let prenotazioniConfermatePerQuelGiorno = this.reservations.filter(i => i.confirm == true && moment(i.dataOraIn) < moment(giorno) && moment(i.dataOraOut) > moment(giorno))
    return prenotazioniConfermatePerQuelGiorno.length
  }

  arrayDaInserireInData() {
    this.daysX.forEach(element => {
      this.valueY.push(this.contaPrenotazioniGiorno(moment([moment().year(), moment().month() , element])));
    });
  }

  //////////////////////////////////////////////////


  delete(stanza: Room) {
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this stanza?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Delete', handler: () => {
          this.roomService.deleteStanza(stanza).subscribe(() => window.location.reload());
        }
      }
      ]
    }).then(alertEl => { alertEl.present() })
  }

  totalePostiperStanza(stanza: Room) {
    return this.desks.filter(i => i.idRoom == stanza.idRoom).length

  }

  postiDisponibili(stanza: Room) {
    let deskInRoom = this.desks.filter(i => i.idRoom == stanza.idRoom);
    let reservationNow = this.reservations.filter(i => moment(i.dataOraIn) < moment() && moment(i.dataOraOut) > moment())
    let x = 0;
    deskInRoom.forEach(element1 => {
      reservationNow.forEach(element2 => {
        if (element1.idDesk == element2.idDesk) {
          x = x + 1;
        }
      });
    });
    return deskInRoom.length - x;
  }

  add() {
    this.router.navigate(['new-room'])
  }
  modify(stanza: Room) {
    localStorage.setItem('idStanzaAdmin', stanza.idRoom.toString())
    this.router.navigate(['lista-desk-admin'])
  }

  updateSede() {
    this.office.city = this.formUpdateSede.get('citta').value,
      this.office.street = this.formUpdateSede.get('via').value,
      this.office.civicNumber = this.formUpdateSede.get('civico').value,
      this.office.cap = this.formUpdateSede.get('cap').value,
      this.office.telefone = this.formUpdateSede.get('telefono').value
    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to update this office?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Modify', handler: () => {
          this.serviceOffice.updateOffice(this.office).subscribe(res => this.router.navigate(['lista-sedi-admin']));
        }
      }]
    }).then(alertEl => { alertEl.present() })
  }
}
