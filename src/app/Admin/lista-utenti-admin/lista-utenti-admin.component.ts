import { Component, DoCheck, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Company } from 'src/app/model/company';
import { User } from 'src/app/model/user';
import { UsersService } from 'src/app/services/users.service';



@Component({
  selector: 'app-lista-utenti-admin',
  templateUrl: './lista-utenti-admin.component.html',
  styleUrls: ['./lista-utenti-admin.component.scss'],
})
export class ListaUtentiAdminComponent implements OnInit, DoCheck {
  companies:Company[]
  users:User[] =[]
  constructor(private service:UsersService,
    private alertCtrl:AlertController,
     private router:Router,
     private _route:ActivatedRoute) { 
      this.companies=this._route.snapshot.data['companies'];
      this.users=this._route.snapshot.data['users'];
     }

  ngOnInit() {
    this.companies=this._route.snapshot.data['companies'];
     this.users=this._route.snapshot.data['users'];
  }
  ngDoCheck(){
      
     this.users=this._route.snapshot.data['users'];
       
  }

  delete(utente:User){
    this.alertCtrl.create({
      header:'Are you sure?',
       message:'Do you really want to delete this user?',
        buttons:[{
          text:'Cancel', role:'cancel'},
          {text:'Delete', handler:()=>{
            this.service.deleteUser(utente);

            let index = this.users.indexOf(utente);
  if(index>=0){
   this.users.splice(index,1);//index=da dove parto e i= la quantità di elementi da eliminare 
  }

          }}
        ]
      }).then(alertEl=>{alertEl.present()})
    
  }

  modifica(idUserAdmin:number){
    localStorage.setItem('idUserAdmin',idUserAdmin.toString())
    this.router.navigate(['user-detail'])

  }
  storico(idUserAdmin:number){
    localStorage.setItem('idUserAdmin',idUserAdmin.toString())
    this.router.navigate(['storico-utente-admin'])

  }
  findCompany(idCompany:number){
   return this.companies.find(i=>i.id==idCompany)
  }
  
}
