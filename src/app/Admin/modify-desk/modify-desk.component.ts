import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Desk } from 'src/app/model/desk';
import { DeskService } from '../../services/desk.service';

@Component({
  selector: 'app-modify-desk',
  templateUrl: './modify-desk.component.html',
  styleUrls: ['./modify-desk.component.scss'],
})
export class ModifyDeskComponent implements OnInit{
  desks:Desk[]
  desk:Desk
  form
  constructor(private serviceDesk:DeskService,
    private router:Router,
    private alertCtrl:AlertController,
    private _route:ActivatedRoute ) {
      this.desks=this._route.snapshot.data['desks']
     }  

  ngOnInit(){
    this.desk=this.desks.find(i=>i.idDesk===+localStorage.getItem('idDeskAdmin')) ;
     this.form = [
    { val: 'WIFI', isChecked:this.desk.wifi },
    { val: 'Rete Lan', isChecked: this.desk.lanConnection },
    { val: 'Stampante', isChecked: this.desk.printer},
    { val: 'Corrente', isChecked: this.desk.electricCurrent },
    { val: 'Monitor', isChecked: this.desk.monitor },
    { val: 'Tastiera', isChecked: this.desk.keyboard },
    { val: 'Mouse', isChecked: this.desk.mouse },
    { val: 'Plotter', isChecked: this.desk.plotter },
  ];
  }


  save(){
this.serviceDesk.updateOpzionDesk(this.form,this.desk)
this.alertCtrl.create({
  header:'Scrivania aggiornata'         
  }).then(alertEl=>{alertEl.present()})

   this.router.navigate(['lista-aziende-admin'])
  }

 

}
