import { Component} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from 'src/app/services/company.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.scss'],
})
export class NewCompanyComponent {
  formAzienda: FormGroup;
  constructor(public fbAzienda: FormBuilder,
    private companyservice: CompanyService,
    private router: Router
  ) {
    this.formAzienda = fbAzienda.group({
      'nameAzienda': ['', [Validators.required, Validators.minLength(2)]],
    })
  }
  addNewAzienda() {
    this.companyservice.addAzienda({
      id: 0,
      name: this.formAzienda.get('nameAzienda').value,
    }).subscribe(res=>this.router.navigate(['lista-aziende-admin']))    


  }
}
