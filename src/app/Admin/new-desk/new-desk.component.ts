import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DeskService } from 'src/app/services/desk.service';

@Component({
  selector: 'app-new-desk',
  templateUrl: './new-desk.component.html',
  styleUrls: ['./new-desk.component.scss'],
})
export class NewDeskComponent {
  formDesk: FormGroup;

  constructor(public fbDesk: FormBuilder,
    private serviceDesk: DeskService,
    private router: Router,
  ) {
    this.formDesk = fbDesk.group({
      'nameDesk': ['', [Validators.required, Validators.minLength(2)]],
    })
  }

  addNewDesk() {
    this.serviceDesk.addDesk({
      idDesk: 0,
      idRoom: +localStorage.getItem('idStanzaAdmin'),
      name: this.formDesk.get('nameDesk').value,
      availability: true,
      description: 'Nessuna descrizione',
      wifi: true,
      lanConnection: true,
      printer: true,
      electricCurrent: true,
      monitor: true,
      keyboard: true,
      mouse: true,
      plotter: true
    }).subscribe(()=>this.router.navigate(['lista-desk-admin']));
    

  }

}
