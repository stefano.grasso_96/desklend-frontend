import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OfficeService } from 'src/app/services/office.service';

@Component({
  selector: 'app-new-office',
  templateUrl: './new-office.component.html',
  styleUrls: ['./new-office.component.scss'],
})
export class NewOfficeComponent  {
  formSede: FormGroup;
  sedi
  constructor(public fbSede: FormBuilder,
    private office: OfficeService,
    private router: Router,
    private _route:ActivatedRoute) {
    this.sedi=this._route.snapshot.data['offices']
    this.formSede = fbSede.group({
      'cittaSede': ['', [Validators.required, Validators.minLength(2)]],
      'viaSede': ['', [Validators.required, Validators.minLength(2)]],
      'civico': ['', [Validators.required, Validators.minLength(1)]],
      'cap': ['', [Validators.required, Validators.minLength(2)]],
      'telefono': ['', [Validators.required, Validators.minLength(2)]],
    })
  }

  addNewSede() {
    this.office.addSede({
      idCompany: + localStorage.getItem('idAziendaAdmin'),
      idOffice: 0,
      city: this.formSede.get('cittaSede').value,
      street: this.formSede.get('viaSede').value,
      civicNumber: this.formSede.get('civico').value,
      cap: this.formSede.get('cap').value,
      telefone: this.formSede.get('telefono').value
    }).subscribe(()=>this.router.navigate(['lista-sedi-admin']));
    
    
  }

}
