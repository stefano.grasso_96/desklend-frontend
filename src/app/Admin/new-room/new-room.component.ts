import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { RoomsService } from 'src/app/services/rooms.service';

@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.scss'],
})
export class NewRoomComponent  {
  formStanza: FormGroup;
  error: string
  toUpload: any = new Array()
  progress: number = 0;
  
  constructor(
    public fbStanza: FormBuilder,
    private roomService: RoomsService,
    private router: Router,
    public http: HttpClient,
    
  ) {
    this.formStanza = fbStanza.group({
      'nameStanza': ['', [Validators.required, Validators.minLength(2)]],
    })
    
  }
  

  addNewStanza() {
    this.roomService.addStanza({
      idOffice: +localStorage.getItem('idSedeAdmin'),
      idRoom: 0,
      name: this.formStanza.get('nameStanza').value
    }).subscribe(()=>this.router.navigate(['lista-stanze-admin']));
    
  }
  //event è l'array di file 
  onDrag(event) {
    if (event.length > 1) {
      this.error = "*Non è possibile caricare più di un file.";
    } else {
      let fileName = event[0].name;
      let split = fileName.split(".");
      let ext = split[split.length - 1].toLowerCase();
      if (ext != "svg") {
        this.error = "*Il file caricato non risulta essere un file SVG";
      } else {
        if (event[0].size > 28000000) {
          this.error = "*Il file è troppo grande, si prega di comprimerlo. (Max 12MB)";
        } else {
          this.toUpload.push(event[0]);
          this.error = null;
        }
      }
    }
  }

  send(): void {
    this.progress = 0;
    let formData = new FormData();
    formData.append('file', this.toUpload[0], this.toUpload[0].name);
    formData.append('request', "UPLOAD");
    this.error = null;
    this.http.post('url sito', formData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(events => {
        if (events.type == HttpEventType.UploadProgress) {
          this.progress = Math.round(events.loaded / events.total * 100);
        } else if (events.type === HttpEventType.Response) {
          let res = events.body;
          if (res[0] == "OK") {
            this.toUpload = new Array();
            this.error = "File caricato con successo!";
          } else {
            this.error = res[1];
          }
        }
      });

  }
}
