import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Company } from 'src/app/model/company';
import { Desk } from 'src/app/model/desk';
import { Reservation } from 'src/app/model/reservation';
import { User } from 'src/app/model/user';


@Component({
  selector: 'app-storico-prenotazioni-admin',
  templateUrl: './storico-prenotazioni-admin.component.html',
  styleUrls: ['./storico-prenotazioni-admin.component.scss'],
})
export class StoricoPrenotazioniAdminComponent implements OnInit {
  reservations: Array<Reservation> = []
  searchTerm
  companies: Company[]
  users: User[]
  desks:Desk[]
  constructor(
    private _route: ActivatedRoute) {
    this.companies = this._route.snapshot.data['companies']
    this.users = this._route.snapshot.data['users']
    this.reservations = this._route.snapshot.data['reservations']
    this.desks=this._route.snapshot.data['desks']
  }

  ngOnInit() {
    this.reservations = this.reservations.sort((a, b) => {
      if (a.idReservation < b.idReservation) {
        return 1;
      }

      if (a.idReservation > b.idReservation) {
        return -1;
      }

      return 0;
    })
  }
  cercaUtente(idUtente) {
    return this.users.find(i => i.id == idUtente)
  }
  cercaAzienda(idUtente) {
    let idA = this.cercaUtente(idUtente).idCompany
    return this.companies.find(i => i.id == idA)
  }
  cercaDesk(idDesk) {
    return this.desks.find(i=>i.idDesk==idDesk)
  }

}
