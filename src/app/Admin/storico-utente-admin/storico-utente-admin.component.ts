import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Desk } from 'src/app/model/desk';
import { Office } from 'src/app/model/office';
import { Reservation } from 'src/app/model/reservation';
import { Room } from 'src/app/model/room';

@Component({
  selector: 'app-storico-utente-admin',
  templateUrl: './storico-utente-admin.component.html',
  styleUrls: ['./storico-utente-admin.component.scss'],
})
export class StoricoUtenteAdminComponent implements OnInit {
  reservationsUser: Array<Reservation> = []
  officies: Office[]
  rooms: Room[]
  desks: Desk[]
  searchTerm
  constructor(private _route: ActivatedRoute) {
    this.officies = this._route.snapshot.data['officies']
    this.rooms = this._route.snapshot.data['rooms']
    this.reservationsUser = this._route.snapshot.data['reservations']
    this.desks = this._route.snapshot.data['desks']
  }

  ngOnInit() {
    this.reservationsUser = this.getReservationForHistory()
  }
  findDesk(idDesk) {
    return this.desks.find(i => i.idDesk == idDesk)
  }

  findOffice(idDesk) {
    let idRoom = this.desks.find(i => i.idDesk == idDesk).idRoom
    let idOffice = this.rooms.find(i => i.idRoom == idRoom).idOffice
    return this.officies.find(i => i.idOffice == idOffice)
  }

  getReservationForHistory(): Array<Reservation> {
    return this.reservationsUser.filter((i) => i.idUser == +localStorage.getItem('idUserAdmin')).sort((a, b) => {
      if (a.idReservation < b.idReservation) {
        return 1;
      }
      if (a.idReservation > b.idReservation) {
        return -1;
      }
      return 0;
    })
  }
}