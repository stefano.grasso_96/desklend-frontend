import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { User } from '../../model/user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
  user: User
  users:User[]
  form: FormGroup;
  constructor(private serviceUser: UsersService,
     public fb: FormBuilder,
      private router: Router,
       private alertCtrl: AlertController,
       private _route:ActivatedRoute) {
         this.users=this._route.snapshot.data['users']
    this.form = fb.group({
      'name': ['', [Validators.required, Validators.minLength(2)]],
      'lastName': ['', [Validators.required, Validators.minLength(2)]],      
      'pw': ['', [Validators.required, Validators.minLength(6)]],
      'mail': ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  ngOnInit() {
    this.user = this.users.find(i=>i.id==+localStorage.getItem('idUserAdmin'))
    this.form = this.fb.group({
      'name': [this.user.name, [Validators.required, Validators.minLength(2)]],
      'lastName': [this.user.lastName, [Validators.required, Validators.minLength(2)]],
      'pw': [this.user.password, [Validators.required, Validators.minLength(6)]],
      'mail': [this.user.mail, [Validators.required, Validators.minLength(6)]]
    })
  }


  save() {
    this.user.name = this.form.get('name').value,
      this.user.lastName = this.form.get('lastName').value,
      this.user.password = this.form.get('pw').value,
      this.user.mail = this.form.get('mail').value

    this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to modify this user?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      },
      {
        text: 'Modify', handler: () => {
          this.serviceUser.updateUser(this.user).subscribe(res=>this.router.navigate(['lista-utenti-admin']));
          
        }
      }]
    }).then(alertEl => { alertEl.present() })
  }

}
