import { Component, DoCheck, OnInit } from '@angular/core';
import { ReservationService } from '../../services/reservation.service';
import { Reservation } from '../../model/reservation';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Room } from 'src/app/model/room';
import { Desk } from 'src/app/model/desk';

@Component({
  selector: 'app-cartina-stanza',
  templateUrl: './cartina-stanza.component.html',
  styleUrls: ['./cartina-stanza.component.scss'],
})
export class CartinaStanzaComponent implements OnInit,DoCheck {
  rooms: Room[]
  desks: Desk[]
  confermaPrenotazione = true;
  titleRoom = 'nome della stanza'
  titleDesk = 'nome della scrivania'
  optionsDesk: Desk
  reservations: Array<Reservation> = []
  dataIn = localStorage.getItem('time In')
  dataOut = localStorage.getItem('time Out')
  SVGFile1: SafeUrl
  //private updateSubscription: Subscription;
  constructor(
    private serviceReservation: ReservationService,
    private router: Router,
    protected sanitizer: DomSanitizer,
    private _route: ActivatedRoute) {
    this.rooms = this._route.snapshot.data['rooms']
    this.reservations = this._route.snapshot.data['reservations']
    this.desks = this._route.snapshot.data['desks']
  }

  ngOnInit() {    
    this.SVGFile1 = this.sanitizer.bypassSecurityTrustResourceUrl('assets/Room_' + localStorage.getItem('idStanza') + '.svg');
    this.titleRoom = this.rooms.find(i => i.idRoom == +localStorage.getItem('idStanza')).name////////////
    this.titleDesk = this.desks.find(i => i.idDesk == +localStorage.getItem('idDesk')).name
    this.optionsDesk = this.desks.find(i => i.idDesk == +localStorage.getItem('idDesk'))
    this.colorDomSvg()
  }
  ngDoCheck(){    
  this.colorDomSvg()
  }

  colorDomSvg() {
    let x = this.serviceReservation.checkPrenotazioni(localStorage.getItem('time In'), localStorage.getItem('time Out'), this.reservations);
    let img = document.getElementById('idObjectSVG1')
    let iframe = (<HTMLObjectElement>img)?.contentDocument;//////
    x.forEach(prenotazione => {
      if (prenotazione.confirm) {
        (<HTMLElement>iframe?.getElementById(prenotazione.idDesk.toString()))?.setAttribute('fill', "red");
      } else {
        (<HTMLElement>iframe?.getElementById(prenotazione.idDesk.toString()))?.setAttribute('fill', "orange");
      }
    })
  }
  save() {
    let newReservation = {
      idReservation: 0,
      idDesk: +localStorage.getItem('idDesk'),
      idUser: +localStorage.getItem('idUser'),
      dataOraIn: moment(localStorage.getItem('time In')).format(),
      dataOraOut: moment(localStorage.getItem('time Out')).format(),
      confirm: false
    }
    
    this.serviceReservation.addReservation(newReservation)    
   
    this.reservations.push(newReservation)
    
    this.confermaPrenotazione = false;
  }
 
  return() {
    this.router.navigate(['app-lista-prenotazioni-attive'])
  }

}
