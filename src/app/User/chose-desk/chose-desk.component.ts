import { Component, DoCheck, NgZone, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Desk } from 'src/app/model/desk';
import { ReservationService } from 'src/app/services/reservation.service';
import { Reservation } from './../../model/reservation';

@Component({
  selector: 'app-chose-desk',
  templateUrl: './chose-desk.component.html',
  styleUrls: ['./chose-desk.component.scss'],
})
export class ChoseDeskComponent implements OnInit, DoCheck {
  reservations: Reservation[]
  desks: Array<Desk> = []
  SVGFile: SafeUrl
  private updateSubscription: Subscription;

  constructor(private serviceReservation: ReservationService,
    private router: Router,
    protected sanitizer: DomSanitizer,
    private _route: ActivatedRoute,
    private zone:NgZone) {
    this.reservations = this._route.snapshot.data['reservations']
    this.desks = this._route.snapshot.data['desks']
  
  }

  ngOnInit() {
   this.onClick(+localStorage.getItem('idStanza'));
   this.SVGFile = this.sanitizer.bypassSecurityTrustResourceUrl('assets/Room_' + localStorage.getItem('idStanza') + '.svg');
     
    
  }
  ngDoCheck() {
   this.colorDomSvg();
  }
  //////////////////////////////////////////////////////TOGLIE DA TUTTI I DESK DELLA STANZA, QUELLI CHE SONO STATI PRENOTATI///////////////////
  onClick(idStanza) {
    let x = this.serviceReservation.checkPrenotazioni(localStorage.getItem('time In'), localStorage.getItem('time Out'), this.reservations)
    this.desks = this.desks.filter(i => i.idRoom == idStanza)
    x.forEach(prenotazione => {
      this.desks.forEach(element => {
        if (prenotazione.idDesk == element.idDesk) {
          let index = this.desks.indexOf(element)
          this.desks.splice(index, 1)
        }
      })
    })
    console.log("this.desks",this.desks)
    return this.desks
  }
  /////////////////////////////////////modifica SVG//////////////////
  colorDomSvg() {
    //prenotazioni 
    let x = this.serviceReservation.checkPrenotazioni(localStorage.getItem('time In'), localStorage.getItem('time Out'), this.reservations);
    let y = this.desks.filter(i => i.idRoom == +localStorage.getItem('idStanza'))

    //rimuove (da tutte le scrivanie della stanza) quelle occupate 
    x.forEach(prenotazione => {
      y = y.filter(i => i.idDesk != prenotazione.idDesk)

    })
    let img = document.getElementById('idObjectSVG')

    let iframe = (<HTMLObjectElement>img).contentDocument;
    //colora di rosso le postazioni PRENOTATE
    x.forEach(prenotazione => {
      if (prenotazione.confirm) {
        (<HTMLElement>iframe?.getElementById(prenotazione.idDesk.toString()))?.setAttribute('fill', "red");
      } else {
        (<HTMLElement>iframe?.getElementById(prenotazione.idDesk.toString()))?.setAttribute('fill', "orange");
      }
    })
    //aggiunge l'evento click alle scrivanie libere
    y.forEach(desk => {
      (<HTMLElement>iframe?.getElementById(desk.idDesk.toString()))?.addEventListener("click", (e: Event) => this.choseRoom(desk.idDesk))
      
    })
  }

  choseRoom(selectedOption) {
    localStorage.setItem('idDesk', selectedOption)
    this.zone.run(()=>{this.router.navigate(['cartina-stanza']);})
    
  }

}




