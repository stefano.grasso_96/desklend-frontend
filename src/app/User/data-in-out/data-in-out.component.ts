import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Desk } from 'src/app/model/desk';
import { Reservation } from 'src/app/model/reservation';
import { Room } from 'src/app/model/room';

@Component({
  selector: 'app-data-in-out',
  templateUrl: './data-in-out.component.html',
  styleUrls: ['./data-in-out.component.scss'],
})
export class DataInOutComponent implements OnInit {
  prosegui = false
  stanze: Array<Room> = []
  prenotazioni: Array<Reservation> = []
  desks: Array<Desk> = []
  myDateTimeIn: Date
  myDateTimeOut: Date
  selectedOption: any

  constructor(
    private router: Router,
    private alertCtrl: AlertController,
    private _route: ActivatedRoute) {
    this.stanze = this._route.snapshot.data['rooms']
  }

  ngOnInit() {
    this.stanze = this.stanze.filter(i => i.idOffice === +localStorage.getItem('idSede'))
  }

  onChose(idRoom) {
    localStorage.setItem('idStanza', idRoom)
    this.prosegui = false
    this.router.navigate(['chose-desk'])
  }

  confirmDatatime() {
    localStorage.setItem('time In', this.myDateTimeIn.toString())
    localStorage.setItem('time Out', this.myDateTimeOut.toString())
    this.prosegui = true
    this.alertCtrl.create({
      header: 'Periodo Settato'
    }).then(alertEl => { alertEl.present() })

  }

}
