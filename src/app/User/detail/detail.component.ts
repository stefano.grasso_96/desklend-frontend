import { Component, OnInit } from '@angular/core';
import { Reservation } from '../../model/reservation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  reservationsUser
  reservations: Reservation[]
  constructor(
    private _route: ActivatedRoute) {
    this.reservations = this._route.snapshot.data['reservations']
  }

  ngOnInit() {
    this.reservationsUser = this.getReservationForHistory().find(i => i.idReservation == +localStorage.getItem('idReservation'))

  }
  getReservationForHistory(): Array<Reservation> {
    return this.reservations.filter((i) => i.idUser == +localStorage.getItem('idUser')).sort((a, b) => {
      if (a.idReservation < b.idReservation) {
        return 1;
      }
      if (a.idReservation > b.idReservation) {
        return -1;
      }
      return 0;
    })
  }

}
