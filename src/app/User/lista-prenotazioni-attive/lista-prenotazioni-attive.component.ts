import { Component, DoCheck, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Desk } from 'src/app/model/desk';
import { Office } from 'src/app/model/office';
import { Reservation } from 'src/app/model/reservation';
import { Room } from 'src/app/model/room';


@Component({
  selector: 'app-lista-prenotazioni-attive',
  templateUrl: './lista-prenotazioni-attive.component.html',
  styleUrls: ['./lista-prenotazioni-attive.component.scss'],
})
export class ListaPrenotazioniAttiveComponent implements OnInit, DoCheck {
  reservationsUser: Reservation[]=[]
  officies: Office[]=[]
  rooms: Room[]=[]
  desks: Desk[]=[]
  constructor(
    private _activatedRoute: ActivatedRoute) {
  }

 
  ngOnInit() {
   
    

    this.officies = this._activatedRoute.snapshot.data['officies']
    this.rooms = this._activatedRoute.snapshot.data['rooms']
    this.desks = this._activatedRoute.snapshot.data['desks']

    this.reservationsUser = this._activatedRoute.snapshot.data['reservations']
    this.reservationsUser = this.reservationsUser.filter((i) => (i.idUser == +localStorage.getItem('idUser')) && (moment(i.dataOraOut) > moment()))
    console.log(this.reservationsUser)
  }

  ngDoCheck() {
    this.reservationsUser = this._activatedRoute.snapshot.data['reservations']
    this.reservationsUser = this.reservationsUser.filter((i) => (i.idUser == +localStorage.getItem('idUser')) && (moment(i.dataOraOut) > moment()))
  }

  findDesk(idDesk) {
    return this.desks.find(i => i.idDesk == idDesk)
  }
  findOffice(idDesk) {
    let idRoom = this.desks.find(i => i.idDesk == idDesk).idRoom
    let idOffice = this.rooms.find(i => i.idRoom == idRoom).idOffice
    return this.officies.find(i => i.idOffice == idOffice)
  }
}

