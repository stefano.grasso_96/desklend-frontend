import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Office } from 'src/app/model/office';
import { OfficeService } from 'src/app/services/office.service';

@Component({
  selector: 'app-scelta',
  templateUrl: './scelta.component.html',
  styleUrls: ['./scelta.component.scss'],
})
export class SceltaSedeComponent implements OnInit {
  sediUser: Array<Office> = []

  constructor(private office: OfficeService,
    private _route:ActivatedRoute) {
      this.sediUser=this._route.snapshot.data['officies']
     }

  ngOnInit() {
    this.sediUser = this.sediUser.filter(i=> i.idCompany === +(localStorage.getItem('idAzienda')))
  }

  setIdSede(id: number) {
    localStorage.setItem('idSede', id.toString())
  }

}
