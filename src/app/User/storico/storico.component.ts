import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Desk } from 'src/app/model/desk';
import { Office } from 'src/app/model/office';
import { Reservation } from 'src/app/model/reservation';
import { Room } from 'src/app/model/room';


@Component({
  selector: 'app-storico',
  templateUrl: './storico.component.html',
  styleUrls: ['./storico.component.scss'],
})
export class StoricoComponent implements OnInit {
  reservationsUser: Array<Reservation> = []
  officies: Office[]
  rooms: Room[]
  desks: Desk[]
  searchTerm
  constructor(private router: Router,
    private _route: ActivatedRoute) {
    this.officies = this._route.snapshot.data['officies']
    this.rooms = this._route.snapshot.data['rooms']
    this.reservationsUser = this._route.snapshot.data['reservations']
    this.desks = this._route.snapshot.data['desks']
  }

  ngOnInit() {
    this.reservationsUser = this.getReservationForHistory()

  }
  findDesk(idDesk) {
    return this.desks.find(i => i.idDesk == idDesk)
  }

  findOffice(idDesk) {
    let idRoom = this.desks.find(i => i.idDesk == idDesk).idRoom
    let idOffice = this.rooms.find(i => i.idRoom == idRoom).idOffice
    return this.officies.find(i => i.idOffice == idOffice)
  }
  details(i) {
    localStorage.setItem('idReservation', i.idReservation)
    this.router.navigate(['detail'])

  }
  getReservationForHistory(): Array<Reservation> {
    return this.reservationsUser.filter((i) => i.idUser == +localStorage.getItem('idUser')).sort((a, b) => {
      if (a.idReservation < b.idReservation) {
        return 1;
      }
      if (a.idReservation > b.idReservation) {
        return -1;
      }
      return 0;
    })
  }

}
