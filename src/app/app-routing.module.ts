import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CartinaStanzaComponent } from './User/cartina-stanza/cartina-stanza.component';

import { DetailComponent } from './User/detail/detail.component';

import { HomePage } from './home/home.page';
import { LoginComponent } from './login/login.component';

import { ModifyDeskComponent } from './Admin/modify-desk/modify-desk.component';
import { NewUserComponent } from './new-user/new-user.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RouteGuardAdminService } from './services/route-guard-admin.service';
import { RouteGuardService } from './services/route-guard.service';
import { UserDetailComponent } from './Admin/user-detail/user-detail.component';
import { AdminHComponent } from './Admin/admin-h/admin-h.component';
import { ListaAziendeAdminComponent } from './Admin/lista-aziende-admin/lista-aziende-admin.component';
import { ListaSediAdminComponent } from './Admin/lista-sedi-admin/lista-sedi-admin.component';
import { ListaStanzeAdminComponent } from './Admin/lista-stanze-admin/lista-stanze-admin.component';
import { ListaDeskAdminComponent } from './Admin/lista-desk-admin/lista-desk-admin.component';
import { ListaUtentiAdminComponent } from './Admin/lista-utenti-admin/lista-utenti-admin.component';
import { ListaPrenotazioniAdminComponent } from './Admin/lista-prenotazioni-admin/lista-prenotazioni-admin.component';
import { StoricoPrenotazioniAdminComponent } from './Admin/storico-prenotazioni-admin/storico-prenotazioni-admin.component';
import { ListaPrenotazioniAttiveComponent } from './User/lista-prenotazioni-attive/lista-prenotazioni-attive.component';
import { StoricoComponent } from './User/storico/storico.component';
import { SceltaSedeComponent } from './User/scelta-sede/scelta.component';
import { DataInOutComponent } from './User/data-in-out/data-in-out.component';
import { NewCompanyComponent } from './Admin/new-company/new-company.component';
import { NewOfficeComponent } from './Admin/new-office/new-office.component';
import { NewRoomComponent } from './Admin/new-room/new-room.component';
import { NewDeskComponent } from './Admin/new-desk/new-desk.component';
import { ChoseDeskComponent } from './User/chose-desk/chose-desk.component';
import { DragdropDirective } from './dragdrop.directive';
import { CompanyResolveService } from './services/company-resolve.service';
import { UsersResolveService } from './services/users-resolve.service';
import { OfficeResolveService } from './services/office-resolve.service';
import { RoomsResolveService } from './services/rooms-resolve.service';
import { ReservationResolveService } from './services/reservation-resolve.service';
import { DeskResolveService } from './services/desk-resolve.service';
import { StoricoUtenteAdminComponent } from './Admin/storico-utente-admin/storico-utente-admin.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  
  {
    path: 'new-user',
    component: NewUserComponent,
    resolve:{companies:CompanyResolveService/*,users:UsersResolveService*/}
  },  
  {
    path: 'login',
    component: LoginComponent,
    //resolve:{/*companies:CompanyResolveService,users:UsersResolveService*/}
  },
  
  {
    path: 'app-lista-prenotazioni-attive',
    component: ListaPrenotazioniAttiveComponent,
    
    resolve:{ officies:OfficeResolveService,rooms:RoomsResolveService,reservations:ReservationResolveService,desks:DeskResolveService},
    canActivate:[RouteGuardService],
  },
  {
    path: 'storico',
    component: StoricoComponent,
    canActivate:[RouteGuardService],
    resolve:{ officies:OfficeResolveService,rooms:RoomsResolveService,reservations:ReservationResolveService,desks:DeskResolveService}
  },
  {
    path: 'scelta-sede',
    component: SceltaSedeComponent,
    canActivate:[RouteGuardService],
    resolve:{officies:OfficeResolveService}
  },
  {
    path: 'data-in-out',
    component: DataInOutComponent,
    canActivate:[RouteGuardService],
    resolve:{rooms:RoomsResolveService}
  },
  {
    path: 'cartina-stanza',
    component: CartinaStanzaComponent,
    canActivate:[RouteGuardService],
    resolve:{rooms:RoomsResolveService,reservations:ReservationResolveService,desks:DeskResolveService}
  },
  {
    path: 'admin-h',
    component: AdminHComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{reservations:ReservationResolveService}
  },
  
  {
    path: 'detail',
    component: DetailComponent,
    canActivate:[RouteGuardService],
    resolve:{reservations:ReservationResolveService}
  },
  {
    path: 'chose-desk',
    component: ChoseDeskComponent,
    canActivate:[RouteGuardService],
    resolve:{reservations:ReservationResolveService,desks:DeskResolveService}
  },
  {
    path: 'lista-aziende-admin',
    component: ListaAziendeAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{companies:CompanyResolveService}
  },
  {
    path: 'lista-sedi-admin',
    component: ListaSediAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{companies:CompanyResolveService, officies:OfficeResolveService,desks:DeskResolveService,rooms:RoomsResolveService,reservations:ReservationResolveService}
  },
  {
    path: 'lista-stanze-admin',
    component: ListaStanzeAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{officies:OfficeResolveService,rooms:RoomsResolveService,desks:DeskResolveService,reservations:ReservationResolveService}
  },
  {
    path: 'lista-desk-admin',
    component: ListaDeskAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{rooms:RoomsResolveService,desks:DeskResolveService}
  },
  {
    path: 'lista-utenti-admin',
    component: ListaUtentiAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{companies:CompanyResolveService,users:UsersResolveService}
  },
  {
    path: 'lista-prenotazioni-admin',
    component: ListaPrenotazioniAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{companies:CompanyResolveService,users:UsersResolveService,reservations:ReservationResolveService}
  },
  {
  path: 'storico-utente-admin',
    component: StoricoUtenteAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{users:UsersResolveService,officies:OfficeResolveService,rooms:RoomsResolveService,reservations:ReservationResolveService,desks:DeskResolveService}
  },
  {
    path: 'modify-desk',
    component: ModifyDeskComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{desks:DeskResolveService}
  },
  {
    path: 'user-detail',
    component: UserDetailComponent,
    canActivate:[RouteGuardAdminService],//////////////////////
    resolve:{users:UsersResolveService}
  },
 
  {
    path: 'storico-prenotazioni-admin',
    component: StoricoPrenotazioniAdminComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{companies:CompanyResolveService,users:UsersResolveService,reservations:ReservationResolveService,desks:DeskResolveService}
  },
  {
    path: 'new-company',
    component: NewCompanyComponent,
    canActivate:[RouteGuardAdminService]
  },
  {
    path: 'new-office',
    component: NewOfficeComponent,
    canActivate:[RouteGuardAdminService],
    resolve:{offices:OfficeResolveService}

  },
  {
    path: 'new-room',
    component: NewRoomComponent,
    canActivate:[RouteGuardAdminService]
  },
  {
    path: 'new-desk',
    component: NewDeskComponent,
    canActivate:[RouteGuardAdminService]
  },
  {
    path: '**',
    component: PagenotfoundComponent
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
   declarations: [
  DragdropDirective
  ]
})
export class AppRoutingModule { }
