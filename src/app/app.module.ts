import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DetailComponent } from './User/detail/detail.component';
import { ModifyDeskComponent } from './Admin/modify-desk/modify-desk.component';
import { NewUserComponent } from './new-user/new-user.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { LoginComponent } from './login/login.component';
import { HomePage } from './home/home.page';
import { NavComponent } from './nav/nav.component';
import { UserDetailComponent } from './Admin/user-detail/user-detail.component';
import { FooterComponent } from './footer/footer.component';
import { RouteGuardService } from './services/route-guard.service';
import { AuthService } from './services/auth.service';

import { IonicStorageModule } from '@ionic/storage';
import { AdminHComponent } from './Admin/admin-h/admin-h.component';
import { ListaAziendeAdminComponent } from './Admin/lista-aziende-admin/lista-aziende-admin.component';
import { ListaDeskAdminComponent } from './Admin/lista-desk-admin/lista-desk-admin.component';
import { ListaPrenotazioniAdminComponent } from './Admin/lista-prenotazioni-admin/lista-prenotazioni-admin.component';
import { ListaSediAdminComponent } from './Admin/lista-sedi-admin/lista-sedi-admin.component';
import { ListaStanzeAdminComponent } from './Admin/lista-stanze-admin/lista-stanze-admin.component';
import { ListaUtentiAdminComponent } from './Admin/lista-utenti-admin/lista-utenti-admin.component';
import { StoricoPrenotazioniAdminComponent } from './Admin/storico-prenotazioni-admin/storico-prenotazioni-admin.component';
import { CartinaStanzaComponent } from './User/cartina-stanza/cartina-stanza.component';
import { DataInOutComponent } from './User/data-in-out/data-in-out.component';
import { ListaPrenotazioniAttiveComponent } from './User/lista-prenotazioni-attive/lista-prenotazioni-attive.component';
import { SceltaSedeComponent } from './User/scelta-sede/scelta.component';
import { StoricoComponent } from './User/storico/storico.component';
import { NewCompanyComponent } from './Admin/new-company/new-company.component';
import { NewOfficeComponent } from './Admin/new-office/new-office.component';
import { NewRoomComponent } from './Admin/new-room/new-room.component';
import { NewDeskComponent } from './Admin/new-desk/new-desk.component';
import { ChoseDeskComponent } from './User/chose-desk/chose-desk.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { StoricoUtenteAdminComponent } from './Admin/storico-utente-admin/storico-utente-admin.component';


@NgModule({
  declarations: [	AppComponent,
  AdminHComponent,
  CartinaStanzaComponent,
  DataInOutComponent,
  DetailComponent,
  ListaAziendeAdminComponent,
  ListaDeskAdminComponent,
  ListaPrenotazioniAdminComponent,
  ListaPrenotazioniAttiveComponent,
  ListaSediAdminComponent,
  ListaStanzeAdminComponent,
  ListaUtentiAdminComponent,
  ModifyDeskComponent,
  NewUserComponent,
  PagenotfoundComponent,
  DetailComponent,
  StoricoComponent,
  LoginComponent,
  SceltaSedeComponent,
  HomePage,
  NavComponent,
  UserDetailComponent,
  FooterComponent,
  StoricoUtenteAdminComponent,
  StoricoPrenotazioniAdminComponent,
  NewCompanyComponent,
  NewOfficeComponent,
  NewRoomComponent,
  NewDeskComponent,
  ChoseDeskComponent,
  
    
   ],
  entryComponents: [],
  imports: [BrowserModule,
    
  HttpClientModule,
     IonicModule.forRoot(),
      AppRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      IonicStorageModule.forRoot(),
      RouterModule,
      Ng2SearchPipeModule
      ],
  providers: [RouteGuardService,AuthService, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
