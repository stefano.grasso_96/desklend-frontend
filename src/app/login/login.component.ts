import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { NavComponent } from './../nav/nav.component';
import { AlertController } from '@ionic/angular';
import { User } from '../model/user';
import { Company } from '../model/company';
import jwtDecode from 'jwt-decode';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  users: User[] = []
  companies: Company[]
  log: NavComponent;
  formU: FormGroup;
  idUser: number;
  idAzienda: number;
  constructor(
    public fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private serviceUser: UsersService,
    private alertCtrl: AlertController) {

    
    this.formU = fb.group({
      'email': ['', [Validators.required, Validators.minLength(6), Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,4}$")]],
      'password': ['', [Validators.required, Validators.minLength(6),]]
    })
  }




  async login() {
    await this.serviceUser.checkUser(this.formU.get('email').value, this.formU.get('password').value).toPromise().then(res => {
      localStorage.setItem('JWT', res);
      let token = localStorage.getItem('JWT');
      let decoded = jwtDecode(token);

      this.idUser = decoded['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'];
      this.idAzienda = decoded['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/locality'];
      localStorage.setItem('idUser', this.idUser.toString());
      localStorage.setItem('idAzienda', this.idAzienda.toString());


      this.entrataEffettiva()

    this.alertCtrl.create({
      header: 'Benvenuto'
    }).then(alertEl => { alertEl.present() })
    },
      error => {
        this.alertCtrl.create(
          {
            header: 'Campi sbagliati'
          }
        ).then(alertEl => { alertEl.present() })
      }
    )
    console.log('appena settato jwt',localStorage.getItem('JWT'))

    

  }


  entrataEffettiva() {
    ///////////////////////////////////////
    //SETTA I TOKEN NELLA LOCALSTORAGE
    let result = this.auth.login(this.formU.get('email').value);
    if (result && this.formU.get('email').value == "admin@admin.com") {
      console.log("token in questo momento admin", localStorage.getItem('JWT'))
      //localStorage.setItem('reload','1')
      this.router.navigate(['admin-h'])
    }
    else {
      console.log("token in questo momento", localStorage.getItem('JWT'))
      //localStorage.setItem('reload','1')
      this.router.navigate(['app-lista-prenotazioni-attive'])
    }
    //////////////////////////////////////////
  }

}


