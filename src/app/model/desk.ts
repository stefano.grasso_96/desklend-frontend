export interface Desk {
    idDesk: number;
    idRoom: number;
    name: string;
    availability: boolean;
    description: string;
    wifi: boolean;
    lanConnection: boolean;
    printer: boolean;
    electricCurrent: boolean;
    monitor: boolean;
    keyboard: boolean;
    mouse: boolean;
    plotter: boolean

}
