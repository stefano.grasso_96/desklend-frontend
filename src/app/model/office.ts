export interface Office {
    idCompany:number;
    idOffice:number;
    city:string;
    street:string;
    civicNumber:number;
    cap:number;
    telefone:number
}
