export interface Reservation {
    idReservation:number;
    idDesk:number;
    idUser:number;
    dataOraIn:string;
    dataOraOut:string
    confirm:boolean;
}
