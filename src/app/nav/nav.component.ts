import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  //clickEventSubscription:Subscription;
  public isUserLogged: boolean

  constructor(private auth: AuthService, private router: Router, private location: Location) {
    this.auth.usersignedin.subscribe(() => { this.isUserLogged = true })
    this.auth.userlogout.subscribe(() => { this.isUserLogged = false })
  }

  ngOnInit() {
    this.isUserLogged = this.auth.isUserLogged()
  }
  //dopo che l'utente fa il logout bisogna rimandarlo alla pagina di login
  logout() {
    this.auth.logout();
    this.router.navigate(['/']);
  }

  login() {
    this.router.navigate(['login']);
    this.isUserLogged = this.auth.isUserLogged()
  }

  back() {
    this.location.back()
  }

  home() {
    if (!!localStorage.getItem('emailAdmin')) {
      return this.router.navigate(['admin-h'])
    }
    if (!!!localStorage.getItem('emailAdmin') && !!localStorage.getItem('emailUser')) {
      return this.router.navigate(['app-lista-prenotazioni-attive'])
    }
    return this.router.navigate(['home'])
  }
}
