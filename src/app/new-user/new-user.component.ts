import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Company } from '../model/company';
import { User } from '../model/user';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
})
export class NewUserComponent {
  //users: User[] = []
  company: Company[] = []
  utente: User
  selectedOption: any

  formU: FormGroup;
  constructor(
    public fb: FormBuilder,
    private service: UsersService,
    private _route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController) {
    this.company = this._route.snapshot.data['companies'];
    //this.users = this._route.snapshot.data['users'];
    this.formU = fb.group({
      'name': ['', [Validators.required, Validators.minLength(2)]],
      'lastname': ['', [Validators.required, Validators.minLength(2)]],
      'email': ['', [Validators.required, Validators.minLength(6), Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,4}$")]],
      'password': ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  addNewUser() {
    //CONTROLLO SE ESISTE GIà UN UTENTE CON QUESTA EMAILs
    
    this.utente = {
      id: 0,
      idCompany: this.company.find(i => i.name === this.selectedOption).id,
      name: this.formU.get('name').value,
      lastName: this.formU.get('lastname').value,
      password: this.formU.get('password').value,
      mail: this.formU.get('email').value
    }


    this.alertCtrl.create({
      header: 'Adding new user',
      message: 'Confirm?',
      buttons: [
        {
          text: 'Confirm', handler: () => {
            this.service.addUser(this.utente).subscribe(res=> {

              if (!!localStorage.getItem('emailAdmin')) {
                this.router.navigate(['/'])
              }
              this.router.navigate(['lista-utenti-admin'])
            },
            error=> this.alertCtrl.create({
              header: 'Are you sure?',
              message: 'This email already exist!!!',
              buttons: [{
                text: 'Cancel', role: 'cancel'
              }]
            }).then(alertEl => { alertEl.present() }));
          }
        }]
    }).then(alertEl => { alertEl.present() })
  }

  checkForm() {
    return this.formU.invalid
  }
}
