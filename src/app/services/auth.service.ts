import { EventEmitter, Injectable, Output } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isUserLoggedIn = false
  private isAdminLoggedIn = false
  //variabili per eventi login,regista e logout
  @Output() usersignedin = new EventEmitter<User>()
  @Output() usersignedup = new EventEmitter()
  @Output() userlogout = new EventEmitter()
  user: User;
  constructor() { }
  //questo metodo potrebbe essere implementato con un osservable perchè domanda al server se l'utente è loggato o no
  // logica per valutare se l'utente è loggato o no
  isUserLogged() {
    //i punti esclamativi sono tipo un cast a bool, se esiste sarà true sennò false
    this.isUserLoggedIn = !!localStorage.getItem('emailUser')
    return this.isUserLoggedIn;
  }
  isAdminLogged() {
    this.isAdminLoggedIn = !!localStorage.getItem('emailAdmin')
    return this.isAdminLoggedIn;
  }
  //metodo con cui l'utente farà il login
  login(email: string) {
    if (email === 'admin@admin.com') {
      localStorage.setItem('emailAdmin', email)
    }
    localStorage.setItem('emailUser', email)
    this.usersignedin.emit()
    //variabile finta che simula il passaggio oggetto utente
    return true;
  }
  //metodo per il logout
  logout() {
    localStorage.clear()
    this.userlogout.emit()
    return this.isUserLoggedIn = false;
  }
}
