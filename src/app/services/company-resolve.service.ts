import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Company } from '../model/company';
import { CompanyService } from './company.service';

@Injectable({
    providedIn: 'root'
  })
export class CompanyResolveService implements Resolve<Company[]> {

constructor(private companyService:CompanyService ) { }

resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Company[]>{
    return this.companyService.getAziende();
  }

}
