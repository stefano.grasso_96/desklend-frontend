import { Injectable } from '@angular/core';
import { Company } from '../model/company';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private myAppUrl = "http://localhost:37781/api/companies"
  aziende: Array<Company> = []
  constructor(private http: HttpClient) { }

  getAziende() {
    return this.http.get<Company[]>(this.myAppUrl)
  }
  updateCompany(company: Company) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

    return this.http.put(this.myAppUrl + "/" + company.id, company,{ headers })
  }
  deleteAzienda(azienda: Company) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

    this.http.delete(this.myAppUrl + "/" + azienda.id,{ headers }).subscribe()
  }
  addAzienda(azienda: Company) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

   return this.http.post(this.myAppUrl, azienda,{ headers })
  }
}
