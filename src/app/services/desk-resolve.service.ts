import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Desk } from '../model/desk';
import { DeskService } from './desk.service';

@Injectable({
  providedIn: 'root'
})
export class DeskResolveService implements Resolve<Desk[]> {

constructor(private deskService:DeskService ) { }

resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Desk[]>{
  return this.deskService.getDesks();
}

}