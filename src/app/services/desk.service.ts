import {  Injectable } from '@angular/core';
import { Desk } from '../model/desk';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeskService {
  private myAppUrl = "http://localhost:37781/api/Desks" 
//LISTA DESK TOTALI
  desks:Array<Desk>=[]

  constructor(private http:HttpClient) { }  
  
  getDesks(){
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }
    return this.http.get<Desk[]>(this.myAppUrl,{ headers })
  }
  
  deleteDesk(desk:Desk){
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

   return this.http.delete(this.myAppUrl+"/"+desk.idDesk,{ headers })
  }
  
  addDesk(desk:Desk){
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

   return this.http.post(this.myAppUrl,desk,{ headers })
  }

  updateOpzionDesk(option,desk:Desk){
   // for(let i=0;i<option.length;i++){}//[i]=option[i].isChecked
    desk.wifi=option[0].isChecked
    desk.lanConnection=option[1].isChecked
    desk.printer=option[2].isChecked
    desk.electricCurrent=option[3].isChecked
    desk.monitor=option[4].isChecked
    desk.keyboard=option[5].isChecked
    desk.mouse=option[6].isChecked
    desk.plotter=option[7].isChecked
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

    this.http.put(this.myAppUrl+"/"+desk.idDesk,desk,{ headers }).subscribe()
   }    
  
 } 


