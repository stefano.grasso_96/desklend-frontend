import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, throwError } from 'rxjs';
import { Office } from '../model/office';
import { OfficeService } from './office.service';
import {catchError, retry} from 'rxjs/operators';
import { Config } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class OfficeResolveService  implements Resolve<Office[]>{
constructor(private officeService:OfficeService) { }


resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Office[]>{
  return this.officeService.getSedi()
}
  

}




