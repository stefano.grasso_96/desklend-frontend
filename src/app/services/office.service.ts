import { Injectable } from '@angular/core';
import { Office } from '../model/office';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/*const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',    
  }).append("Authorization", "Bearer " + localStorage.getItem('JWT')),
};*/

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  private myAppUrl = "http://localhost:37781/api/Offices"
constructor(
    private http:HttpClient) { }

//LISTA SEDI TOTALI
sedi:Array<Office>=[]

getSedi(){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }
  return this.http.get<Office[]>(this.myAppUrl,{ headers })
}

updateOffice(office:Office){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.put(this.myAppUrl+"/"+office.idOffice,office,{ headers })
}

deleteSede(sede:Office){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.delete(this.myAppUrl+"/"+sede.idOffice,{ headers })
}
addSede(sede:Office){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

  return this.http.post(this.myAppUrl,sede,{ headers })
}

}
