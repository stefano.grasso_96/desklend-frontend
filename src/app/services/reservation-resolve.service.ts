import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Reservation } from '../model/reservation';
import { ReservationService } from './reservation.service';

@Injectable({
  providedIn: 'root'
})
export class ReservationResolveService implements Resolve<Reservation[]>{

constructor(private reservationService:ReservationService) { }
resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Reservation[]>{
  return this.reservationService.getReservations();
}

}
