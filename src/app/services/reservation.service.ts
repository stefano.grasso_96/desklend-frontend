import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Reservation } from '../model/reservation';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private myAppUrl ="http://localhost:37781/api/Reservations"
  constructor(private http:HttpClient) { }
  //LISTA PRENOTAZIONI TOTALI
  

  getReservations() {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }
    return this.http.get<Reservation[]>(this.myAppUrl,{ headers })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //tutte le prenotazioni che non possono essere aggiunte
  checkPrenotazioni(inDataUtenteNuovo, outDataUtenteNuovo,reservations) {
    let listaSovrapposizioniReservation = reservations.filter(i =>
      (moment(inDataUtenteNuovo) > moment(i.dataOraIn) && moment(inDataUtenteNuovo) < moment(i.dataOraOut)) ||
      (moment(outDataUtenteNuovo) > moment(i.dataOraIn) && moment(outDataUtenteNuovo) < moment(i.dataOraOut)) ||
      (moment(inDataUtenteNuovo) < moment(i.dataOraIn) && moment(outDataUtenteNuovo) > moment(i.dataOraOut)))
    return listaSovrapposizioniReservation
  }

  deleteReservation(reservation: Reservation) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

   return this.http.delete(this.myAppUrl+"/"+reservation.idReservation,{ headers })   
  }
  confirmReservation(reservation: Reservation) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

     reservation.confirm = true
   this.http.put(this.myAppUrl+"/"+reservation.idReservation,reservation,{ headers }).subscribe()
    
  }
  addReservation(reservation: Reservation) {
    let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

    this.http.post(this.myAppUrl,reservation,{ headers }).subscribe()
  }

}
