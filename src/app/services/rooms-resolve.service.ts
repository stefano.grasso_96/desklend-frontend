import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Room } from '../model/room';
import { RoomsService } from './rooms.service';

@Injectable({
  providedIn: 'root'
})
export class RoomsResolveService  implements Resolve<Room[]>{

constructor(private roomService:RoomsService) { }

resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Room[]>{
  return this.roomService.getStanze()
}

}
