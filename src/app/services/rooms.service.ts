import { Injectable } from '@angular/core';
import { Room } from 'src/app/model/room';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  private myAppUrl = "http://localhost:37781/api/Rooms"
constructor(private http:HttpClient) { }

//LISTA STANZE TOTALI
stanze:Array<Room>=[]


getStanze(){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }
  return this.http.get<Room[]>(this.myAppUrl,{ headers })
}

updateRoom(room:Room){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.put(this.myAppUrl+"/"+room.idRoom,room,{ headers })
}
deleteStanza(stanza:Room){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

return this.http.delete(this.myAppUrl+"/"+stanza.idRoom,{ headers })
}
addStanza(stanza:Room){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.post(this.myAppUrl,stanza,{ headers })
}

}
