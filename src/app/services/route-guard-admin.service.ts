import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardAdminService implements CanActivate {

constructor(private router:Router, private authService:AuthService) { }

canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
    
  if(this.authService.isAdminLogged()){
    return true;
  }else{
    return this.router.navigate(['/']);
  }
 }
}
