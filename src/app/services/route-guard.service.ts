import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {

  constructor(private router:Router, private auth:AuthService) { }
//route per avere i parametri della rotta
  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
    
   if(this.auth.isUserLogged()){
     return true;
   }else{
     return this.router.navigate(['/']);
   }
  }
}
