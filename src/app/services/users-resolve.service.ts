import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { UsersService } from './users.service';

@Injectable({
    providedIn: 'root'
  })
export class UsersResolveService implements Resolve<User[]> {    
    
    constructor(private userService:UsersService ) { }
    
    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<User[]>{
        return this.userService.getUser();
      }
    
    }