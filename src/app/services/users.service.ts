import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { User } from '../model/user';




@Injectable({
  providedIn: 'root'
})
export class UsersService {
loading:boolean=false;
idUser:number;
idAzienda:number;
private myAppUrl = "http://localhost:37781/api/Users"
//LISTA UTENTI TOTALI
users: Array<User> =[]

constructor(public http:HttpClient,
  private alertCtrl: AlertController) { }


getUser(){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

  return this.http.get<User[]>(this.myAppUrl,{ headers })
}

deleteUser(user:User):void{
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

  this.http.delete(this.myAppUrl+"/"+user.id,{ headers }).subscribe()
}
updateUser(user:User){
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.put(this.myAppUrl+"/"+user.id,user,{ headers })
}
addUser(user:User){  
  let headers = { 'Authorization': 'Bearer '+localStorage.getItem('JWT') }

 return this.http.post(this.myAppUrl,user,{ headers })
}

/////////////////////////////////////////////////////////////////////////////////////////
checkUser(email:string, password:string){  
let credentials={
  'email':email,
  'password':password
}
 return this.http.post("http://localhost:37781/api/Login",credentials,{ responseType: 'text' } )
  }
 

}
